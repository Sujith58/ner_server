# !/usr/bin/env python#
import re
import pandas

def read6kArticles(filename='GoodBadAbstractTrainingSet.xlsx'):
    df_good = pandas.read_excel(filename, sheet_name="GOOD")    
    df_bad = pandas.read_excel(filename, sheet_name="BAD")

    df_good['good'] = 1
    df_bad['good'] = 0
    return df_good.append(df_bad)

df_6k_articles = read6kArticles()


from NER_functions import *
MeshTerms, MeshDict = getMeshTermList('Final_MeSHTerms_NewVersion.csv')  # getMesh term list
MeshDict
termMatches = getTermListWithinArticles(df_6k_articles['Abstract Body'], MeshTerms, MeshDict, True, 'MedraTermsIn6kArticles.pickle')
termMatches


#hitCounts = [len(x) for x in termMatches]
#displayHitHistogram(hitCounts, '6k articles')

#displayHitHistogram([len(termMatches[i]) for i,x in enumerate(df_6k_articles['good']) if x == 1],'medical articles')
#displayHitHistogram([len(termMatches[i]) for i,x in enumerate(df_6k_articles['good']) if x != 1],'non-medical articles')


#mostFrequentTerms(termMatches)  
#pprint.pprint(mostFrequentTerms([termMatches[i] for i,x in enumerate(df_6k_articles['good']) if x == 1])[:10])
#print()
#pprint.pprint(mostFrequentTerms([termMatches[i] for i,x in enumerate(df_6k_articles['good']) if x != 1])[:10])


#enlocation = 'C:/ProgramData/Anaconda3/lib/site-packages/en_core_web_sm/en_core_web_sm-2.0.0'


#create training set for Spacy NER

nlp = spacy.load('en') #('en')

print('start time: ' + time.ctime())
start_time = time.time()
matchedMeshTerms, train_data = getMatchesWithinSentences(nlp, df_6k_articles['Abstract Body'], termMatches)
print("--- %s seconds ---" % (time.time() - start_time))

print(Counter(matchedMeshTerms))
#pprint.pprint(train_data[100:110])


from NER_functions import *

def addToNER(LABEL, TRAIN_DATA, model=None, new_model_name='animal', output_dir=None, n_iter=20, test_text='Do you like horses?'):
    nlp = trainModelWithNewEntityType(LABEL, TRAIN_DATA, model=None, n_iter=20)
    
    printEntitiesInText(nlp, test_text)  # test the trained model
  
    if output_dir is not None:
        saveNLPModel(nlp, output_dir, new_model_name)
        
        print("Loading from", output_dir)  # test the saved model
        nlp2 = spacy.load(output_dir)
        printEntitiesInText(nlp2, test_text, 'reloaded ')

if True:   ### STOP. This takes time: it took 15 hours to generate the NER terms from the 1000 articles
    import time
    print('start time: ' + time.ctime())
    start_time = time.time()
    
    #enlocation = 'C:/ProgramData/Anaconda3/lib/site-packages/en_core_web_sm/en_core_web_sm-2.0.0'
    addToNER('MESH',train_data, model='en', output_dir='MeshModelServer061218', test_text="is there anything beyond disease and death?")
    
    print("--- %s seconds ---" % (time.time() - start_time))


# demonstrate the outputs for the mesh NER and the default NER
from NER_functions import *
test_text = df_6k_articles['Abstract Body'].iloc[23].replace('  ',' ')
nlp3 = spacy.load('MeshModelServer061218')
printFullEntityList(nlp3, test_text)
print()
printFullEntityList(spacy.load('en'), test_text)
print(test_text)